import { back_url } from '../../../constants/index'


export const getUsuarioLogeado = async () => {

    try {

        const data = {
            email: "sabici2013@gmail.com",
            password: "123"
        }

        const query = await fetch(`${back_url}/user/login`, {
            method: 'POST', // *GET, POST, PUT, DELETE, etc.
            mode: 'cors', // no-cors, *cors, same-origin
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            credentials: 'same-origin', // include, *same-origin, omit
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            redirect: 'follow', // manual, *follow, error
            referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
            body: JSON.stringify(data) // body data type must match "Content-Type" header
        });


        const json = await query.json();

        return json;


    } catch (error) {
        // manejo del error
        // console.error(error)
        return error;
    }



}

export const getUsuarioSingup = async () => {

    try {
        const data = {
            rut: "177572330",
            email: "sabici2013@gmail.com",
            password: "123",
            nombre: "sergio",
            apellido: "arce",
            telefono: "+569 4904 5275",
            fechaNacimiento: "01-02-05",
            role: "admin"
        }

        const query = await fetch(`${back_url}/user/signup`, {
            method: 'POST', // *GET, POST, PUT, DELETE, etc.
            mode: 'cors', // no-cors, *cors, same-origin
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            credentials: 'same-origin', // include, *same-origin, omit
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            redirect: 'follow', // manual, *follow, error
            referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
            body: JSON.stringify(data) // body data type must match "Content-Type" header
        });

        const json = await query.json();


        return json;


    } catch (error) {
        // manejo del error
        // console.error(error)
        return error;
    }



}