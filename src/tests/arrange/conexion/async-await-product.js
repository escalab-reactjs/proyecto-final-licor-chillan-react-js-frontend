import { back_url } from '../../../constants/index'


export const getOneProduct = async () => {

    try {
        const query = await fetch(`${back_url}/product/getOneProduct/4568`, {
            method: 'GET', // *GET, POST, PUT, DELETE, etc.
            mode: 'cors', // no-cors, *cors, same-origin
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            credentials: 'same-origin', // include, *same-origin, omit
            redirect: 'follow', // manual, *follow, error
            referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
        });

        const json = await query.json();
        console.log("hola mundo esto en un console log");
        console.log(json);
        return json;

    } catch (error) {
        // manejo del error
        console.error(error)
        return 'No existe';
    }
}


export const getAllProducts = async () => {

    try {
        const query = await fetch(`${back_url}/product/getAll`, {
            method: 'GET', // *GET, POST, PUT, DELETE, etc.
            mode: 'cors', // no-cors, *cors, same-origin
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            credentials: 'same-origin', // include, *same-origin, omit
            headers: {
                'x-access-token': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI2MDZlMzBmYWVkZDI3Yjc0MjkzMWQ3NTQiLCJpYXQiOjE2MTc4NDkyMjEsImV4cCI6MTYxNzkzNTYyMX0.Mt2tIVPu3Tm2xZuzOM5YByKjwi5qBdomIYSuYrLxjyo'
            },
            redirect: 'follow', // manual, *follow, error
            referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
        });

        const json = await query.json();
        console.log("hola mundo esto en un console log");
        console.log(json);
        return json;


    } catch (error) {
        // manejo del error
        console.error(error)
        return error;
    }



}


export const updateOneProduct = async () => {

    try {

        const data = {
            name: "pepse",
            stock: "5",
            url_img: "product",
            price_unitary: "5000",
            brand: "ccu"
        }

        const query = await fetch(`${back_url}/product/updateOneProduct/1234587`, {
            method: 'PUT', // *GET, POST, PUT, DELETE, etc.
            mode: 'cors', // no-cors, *cors, same-origin
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            credentials: 'same-origin', // include, *same-origin, omit
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'x-access-token': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI2MDdhNWM2MzUwNjgyYjQwMGIxOGNjMjQiLCJpYXQiOjE2MTg3MTMzNDYsImV4cCI6MTYxODc5OTc0Nn0.ZWNQp9nxSArYeCYSA3iRWlERAFiyuMeALg4xgjfJJ1g'
            },
            redirect: 'follow', // manual, *follow, error
            referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
            body: JSON.stringify(data) // body data type must match "Content-Type" header
        });

        const json = await query.json();
        return json;

    } catch (error) {
        // manejo del error
        console.error(error)
        return error;
    }

}


export const createOneProduct = async () => {

    try {

        const url = "607bcb6e90325d7e05066faf"
        const data = {
            name: "Coca Cola un litro",
            stock: "50",
            url_img: "product",
            price_unitary: "1000",
            brand: "coca cola"
        }

        const query = await fetch(`${back_url}/product/createOneProduct`, {
            method: 'POST', // *GET, POST, PUT, DELETE, etc.
            mode: 'cors', // no-cors, *cors, same-origin
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            credentials: 'same-origin', // include, *same-origin, omit
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'x-access-token': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI2MDdhNWM2MzUwNjgyYjQwMGIxOGNjMjQiLCJpYXQiOjE2MTg3MTMzNDYsImV4cCI6MTYxODc5OTc0Nn0.ZWNQp9nxSArYeCYSA3iRWlERAFiyuMeALg4xgjfJJ1g'

            },
            redirect: 'follow', // manual, *follow, error
            referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
            body: JSON.stringify(data) // body data type must match "Content-Type" header

        });

        const json = await query.json();

        return json;

    } catch (error) {
        // manejo del error
        console.error(error)
        return error;
    }

}

export const deleteOneOnlyProduct = async () => {

    try {

        const data = "607bc758385d2e7d38573027"

        const query = await fetch(`${back_url}/product/deleteOneOnlyProduct/${data}`, {
            method: 'DELETE', // *GET, POST, PUT, DELETE, etc.
            headers: {
                'x-access-token': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI2MDZlMzBmYWVkZDI3Yjc0MjkzMWQ3NTQiLCJpYXQiOjE2MTc4NDkyMjEsImV4cCI6MTYxNzkzNTYyMX0.Mt2tIVPu3Tm2xZuzOM5YByKjwi5qBdomIYSuYrLxjyo'
            },
        });

        const json = await query.json();
        console.log("json" + JSON.stringify(json));

        return json;

    } catch (error) {
        // manejo del error
        console.error(error)
        return error;
    }

}