import { back_url } from '../../../constants/index'


export const getAllPromotion = async () => {

    try {

        const query = await fetch(`${back_url}/promotion/getAllPromotion`, {
            method: 'GET', // *GET, POST, PUT, DELETE, etc.
            mode: 'cors', // no-cors, *cors, same-origin
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            credentials: 'same-origin', // include, *same-origin, omit

            redirect: 'follow', // manual, *follow, error
            referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
        });

        const json = await query.json();
        return json;

    } catch (error) {
        // manejo del error
        console.error(error)
        return 'No existe';
    }
}


export const getActivePromotion = async () => {

    try {

        const query = await fetch(`${back_url}/promotion/getActivePromotion`, {
            method: 'GET', // *GET, POST, PUT, DELETE, etc.
            mode: 'cors', // no-cors, *cors, same-origin
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            credentials: 'same-origin', // include, *same-origin, omit
            headers: {
                'x-access-token': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI2MDdhNWM2MzUwNjgyYjQwMGIxOGNjMjQiLCJpYXQiOjE2MTg3MTMzNDYsImV4cCI6MTYxODc5OTc0Nn0.ZWNQp9nxSArYeCYSA3iRWlERAFiyuMeALg4xgjfJJ1g'
            },
            redirect: 'follow', // manual, *follow, error
            referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
        });
        const json = await query.json();


        return json;

    } catch (error) {
        // manejo del error
        console.error(error)
        return error;
    }



}


// export const createOnePromotionWithImg = async () => {

//     try {

//         const formData = new FormData();
//         formData.append('file', fileInput.files[0]);


//         const query = await fetch(`${back_url}/promotion/createOnePromotionWithImg`, {
//             method: 'POST', // *GET, POST, PUT, DELETE, etc.
//             mode: 'cors', // no-cors, *cors, same-origin
//             cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
//             credentials: 'same-origin', // include, *same-origin, omit
//             headers: {
//                 'Accept': 'application/json',
//                 'Content-Type': 'application/json'
//             },
//             redirect: 'follow', // manual, *follow, error
//             referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
//             body: JSON.stringify(data) // body data type must match "Content-Type" header
//         });

//         const json = await query.json();


//         return json;

//     } catch (error) {
//         // manejo del error
//         console.error(error)
//         return error;
//     }

// }


export const createOnePromotion = async () => {

    try {

        const data = {

            name: "promo prueba",
            from_date: "2021-04-16T00:00",
            to_date: "2021-04-30T00:00",
            active: false,
            cost_promotion: "8000",
            price_promotion: "8500",
            url_img: "promotions",

        }

        const query = await fetch(`${back_url}/promotion/createOnePromotion`, {
            method: 'POST', // *GET, POST, PUT, DELETE, etc.
            mode: 'cors', // no-cors, *cors, same-origin
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            credentials: 'same-origin', // include, *same-origin, omit
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'x-access-token': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI2MDdhNWM2MzUwNjgyYjQwMGIxOGNjMjQiLCJpYXQiOjE2MTg3MTMzNDYsImV4cCI6MTYxODc5OTc0Nn0.ZWNQp9nxSArYeCYSA3iRWlERAFiyuMeALg4xgjfJJ1g'

            },
            redirect: 'follow', // manual, *follow, error
            referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
            body: JSON.stringify(data) // body data type must match "Content-Type" header
        });

        const json = await query.json();
        console.log("json" + JSON.stringify(json));

        return json;

    } catch (error) {
        // manejo del error
        console.error(error)
        return error;
    }

}


export const updateOnePromotion = async () => {

    try {

        const url = "607bcb6e90325d7e05066faf"
        const data = {
            name: "prueba actualizar test",
            from_date: "2021-04-16T00:00",
            to_date: "2021-04-30T00:00",
            active: false,
            cost_promotion: "9000",
            price_promotion: "8500",
            url_img: "promotions actualizar test"
        }

        const query = await fetch(`${back_url}/promotion/updateOnePromotion/${url}`, {
            method: 'PUT', // *GET, POST, PUT, DELETE, etc.
            mode: 'cors', // no-cors, *cors, same-origin
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            credentials: 'same-origin', // include, *same-origin, omit
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'x-access-token': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI2MDdhNWM2MzUwNjgyYjQwMGIxOGNjMjQiLCJpYXQiOjE2MTg3MTMzNDYsImV4cCI6MTYxODc5OTc0Nn0.ZWNQp9nxSArYeCYSA3iRWlERAFiyuMeALg4xgjfJJ1g'

            },
            redirect: 'follow', // manual, *follow, error
            referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
            body: JSON.stringify(data) // body data type must match "Content-Type" header

        });

        const json = await query.json();
        console.log("json" + JSON.stringify(json));

        return json;

    } catch (error) {
        // manejo del error
        console.error(error)
        return error;
    }

}

export const deleteOneOnlyPromotion = async () => {

    try {

        const data = "607bc758385d2e7d38573027"

        const query = await fetch(`${back_url}/promotion/createOnePromotion/${data}`, {
            method: 'DELETE', // *GET, POST, PUT, DELETE, etc.

        });

        const json = await query.json();
        console.log("json" + JSON.stringify(json));

        return json;

    } catch (error) {
        // manejo del error
        console.error(error)
        return error;
    }

}