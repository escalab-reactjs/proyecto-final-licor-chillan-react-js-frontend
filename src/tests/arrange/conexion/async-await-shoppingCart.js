import { back_url } from '../../../constants/index'


export const getOneShoppingCart = async () => {

    try {
        const query = await fetch(`${back_url}/ShoppingCart/getOneShoppingCart/123`, {
            method: 'GET', // *GET, POST, PUT, DELETE, etc.
            mode: 'cors', // no-cors, *cors, same-origin
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            credentials: 'same-origin', // include, *same-origin, omit
            headers: {
                'x-access-token': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI2MDZlMzBmYWVkZDI3Yjc0MjkzMWQ3NTQiLCJpYXQiOjE2MTc4NDkyMjEsImV4cCI6MTYxNzkzNTYyMX0.Mt2tIVPu3Tm2xZuzOM5YByKjwi5qBdomIYSuYrLxjyo'
            },
            redirect: 'follow', // manual, *follow, error
            referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
        });

        const json = await query.json();
        console.log("hola mundo esto en un console log");
        console.log(json);
        return json;

    } catch (error) {
        // manejo del error
        console.error(error)
        return 'No existe';
    }
}


export const getAllShoppingCart = async () => {

    try {
        const query = await fetch(`${back_url}/ShoppingCart/getAllShoppingCart`, {

            method: 'GET', // *GET, POST, PUT, DELETE, etc.
            mode: 'cors', // no-cors, *cors, same-origin
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            credentials: 'same-origin', // include, *same-origin, omit
            headers: {
                'x-access-token': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI2MDZlMzBmYWVkZDI3Yjc0MjkzMWQ3NTQiLCJpYXQiOjE2MTc4NDkyMjEsImV4cCI6MTYxNzkzNTYyMX0.Mt2tIVPu3Tm2xZuzOM5YByKjwi5qBdomIYSuYrLxjyo'
            },
            redirect: 'follow', // manual, *follow, error
            referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
        });

        const json = await query.json();
        console.log("hola mundo esto en un console log");
        console.log(json);
        return json;


    } catch (error) {
        // manejo del error
        console.error(error)
        return error;
    }

}


export const updateOneShoppingCart = async () => {

    try {
        const url = "607bcb6e90325d7e05066faf"
        const data = {
            pertainTo: "pepse",
            state: "5000",
            cost_products: "ccu",
            cost_promotions: "ccu",
            price_final: "ccu",
            promotions: [{
                name: "",
                amount: "",
                cost_promotions: "",
                price_promotion: ""
            }],
            products: [{
                name: "",
                amount: "",
                brand: "",
                url_img: "",
                total_price_products: ""
            }]

        }

        const query = await fetch(`${back_url}/ShoppingCart/updateOneShoppingCart/${url}`, {
            method: 'PUT', // *GET, POST, PUT, DELETE, etc.
            mode: 'cors', // no-cors, *cors, same-origin
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            credentials: 'same-origin', // include, *same-origin, omit
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'x-access-token': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI2MDdhNWM2MzUwNjgyYjQwMGIxOGNjMjQiLCJpYXQiOjE2MTg3MTMzNDYsImV4cCI6MTYxODc5OTc0Nn0.ZWNQp9nxSArYeCYSA3iRWlERAFiyuMeALg4xgjfJJ1g'
            },
            redirect: 'follow', // manual, *follow, error
            referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
            body: JSON.stringify(data) // body data type must match "Content-Type" header
        });

        const json = await query.json();
        return json;

    } catch (error) {
        // manejo del error
        console.error(error)
        return error;
    }

}


export const createOneShoppingCart = async () => {

    try {


        const data = {
            pertainTo: "pepse",
            state: "5000",
            cost_products: "ccu",
            cost_promotions: "ccu",
            price_final: "ccu",
            promotions: [{
                name: "",
                amount: "",
                cost_promotions: "",
                price_promotion: ""
            }],
            products: [{
                name: "",
                amount: "",
                brand: "",
                url_img: "",
                total_price_products: ""
            }]

        }

        const query = await fetch(`${back_url}/ShoppingCart/createOneShoppingCart`, {
            method: 'POST', // *GET, POST, PUT, DELETE, etc.
            mode: 'cors', // no-cors, *cors, same-origin
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            credentials: 'same-origin', // include, *same-origin, omit
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'x-access-token': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI2MDdhNWM2MzUwNjgyYjQwMGIxOGNjMjQiLCJpYXQiOjE2MTg3MTMzNDYsImV4cCI6MTYxODc5OTc0Nn0.ZWNQp9nxSArYeCYSA3iRWlERAFiyuMeALg4xgjfJJ1g'

            },
            redirect: 'follow', // manual, *follow, error
            referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
            body: JSON.stringify(data) // body data type must match "Content-Type" header

        });

        const json = await query.json();

        return json;

    } catch (error) {
        // manejo del error
        console.error(error)
        return error;
    }

}

export const deleteOneOnlyShoppingCart = async () => {

    try {

        const data = "607d0b18139ae55416b2dca7"

        const query = await fetch(`${back_url}/ShoppingCart/deleteOneOnlyShoppingCart/${data}`, {
            method: 'DELETE', // *GET, POST, PUT, DELETE, etc.
            headers: {
                'x-access-token': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI2MDZlMzBmYWVkZDI3Yjc0MjkzMWQ3NTQiLCJpYXQiOjE2MTc4NDkyMjEsImV4cCI6MTYxNzkzNTYyMX0.Mt2tIVPu3Tm2xZuzOM5YByKjwi5qBdomIYSuYrLxjyo'
            },
        });

        const json = await query.json();
        console.log("json" + JSON.stringify(json));

        return json;

    } catch (error) {
        // manejo del error
        console.error(error)
        return error;
    }

}