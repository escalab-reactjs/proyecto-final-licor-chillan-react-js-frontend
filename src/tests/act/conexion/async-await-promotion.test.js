import { getAllPromotion, getActivePromotion, createOnePromotion, deleteOneOnlyPromotion, updateOnePromotion } from "../../arrange/conexion/async-await-promotion"


describe('call to data base of PROMOTION', () => {

    test('testing of the call de getallpromotion', async () => {

        const url = await getAllPromotion();
        console.warn("hola mundo esto es una prueba para un test");
        console.warn(url.data);

        expect(url).toEqual(expect.objectContaining({
            data: expect.anything(),
        }),
        );

    })

    test('testing of the call de getActivePromotion', async () => {

        const url = await getActivePromotion();
        console.warn("hola mundo esto es una prueba para un test");
        console.warn(url.data);

        expect(url).toEqual(expect.objectContaining({
            data: expect.anything(),
        }),
        );

    })

    test('testing of the call de createOnePromotion', async () => {

        const url = await createOnePromotion();
        console.warn("hola mundo esto es una prueba para un test");
        console.warn(url.data);

        expect(url).toEqual(expect.objectContaining({
            data: expect.anything(),
        }),
        );

    })

    test('testing of the call de updateOnePromotion', async () => {

        const url = await updateOnePromotion();
        console.warn("hola mundo esto es una prueba para un test");
        console.warn(url);

        expect(url).toEqual(expect.objectContaining({
            _id: expect.anything(),
        }),
        );

    })

    test('testing of the call de deleteOneOnlyPromotion', async () => {

        const url = await deleteOneOnlyPromotion();
        console.warn("hola mundo esto es una prueba para un test");
        console.warn(url.data);

        expect(url).toEqual(expect.objectContaining({
            message: expect.anything(),
        }),
        );

    })


})