import { getOneCoupon, getAllCoupon, updateOneCoupon, createOneCoupon, deleteOneOnlyCoupon } from "../../arrange/conexion/async-await-coupon"


describe('call to data base of PRODUCT', () => {


    test('testing of the call de getOneCoupon', async () => {

        const url = await getOneCoupon();
        console.warn("hola mundo esto es una prueba para un test");
        console.warn(url.data);

        expect(url).toEqual(expect.objectContaining({
            status: expect.anything(),
        }),
        );

    })

    test('testing of the call de getAllCoupon', async () => {
        const url = await getAllCoupon();

        expect(url).toEqual(expect.objectContaining({
            status: expect.anything(),
        }),

        );
    })


    test('testing of the call de updateOneCoupon', async () => {

        const url = await updateOneCoupon();
        console.warn("hola mundo esto es una prueba para un test");
        console.warn(url.data);

        expect(url).toEqual(expect.objectContaining({
            status: expect.anything(),
        }),
        );

    })

    test('testing of the call de createOneCoupon', async () => {

        const url = await createOneCoupon();
        console.warn("hola mundo esto es una prueba para un test");
        console.warn(url);

        expect(url).toEqual(expect.objectContaining({
            status: expect.anything(),
        }),
        );

    })

    test('testing of the call de deleteOneOnlyCoupon', async () => {

        const url = await deleteOneOnlyCoupon();
        console.warn("hola mundo esto es una prueba para un test");
        console.warn(url.data);

        expect(url).toEqual(expect.objectContaining({
            status: expect.anything(),
            // error: expect.anything()
        }),
        );

    })


})