import { getUsuarioLogeado, getUsuarioSingup } from "../../arrange/conexion/async-await-user"


describe('call to data base of user', () => {
    function randocall(fn) {
        return fn(Math.floor(Math.random() * 6 + 1));
    }
    test('testing of the call de login', async () => {

        const url = await getUsuarioLogeado();
        console.warn("hola mundo esto es una prueba para un test");
        console.warn(url.data);

        expect(url.data).toEqual(expect.objectContaining({
            email: expect.any(String),
        }));
    })


    test('testing of the call de singup', async () => {
        const url = await getUsuarioSingup();

        console.warn("url" + url);

        expect(url).toBeCalledWith(
            expect.objectContaining({
                data: expect.any(Object),

            }),
        );
    });


})