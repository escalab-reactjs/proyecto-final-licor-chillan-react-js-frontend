import { getOneProduct, getAllProducts, updateOneProduct, createOneProduct, deleteOneOnlyProduct } from "../../arrange/conexion/async-await-product"


describe('call to data base of PRODUCT', () => {

    test('testing of the call de getOneProduct', async () => {

        const url = await getOneProduct();
        console.warn("hola mundo esto es una prueba para un test");
        console.warn(url.data);

        expect(url).toEqual(expect.objectContaining({
            status: expect.anything(),
        }),
        );

    })

    test('testing of the call de getAllProducts', async () => {
        const url = await getAllProducts();
        console.warn("hola mundo esto es una prueba para un test");
        console.warn(url.status);

        expect(url).toEqual(expect.objectContaining({
            status: expect.anything(),
        }),

        );
    })


    test('testing of the call de updateOneProduct', async () => {

        const url = await updateOneProduct();
        console.warn("hola mundo esto es una prueba para un test");
        console.warn(url.data);

        expect(url).toEqual(expect.objectContaining({
            status: expect.anything(),
        }),
        );

    })

    test('testing of the call de createOneProduct', async () => {

        const url = await createOneProduct();
        console.warn("hola mundo esto es una prueba para un test");
        console.warn(url);

        expect(url).toEqual(expect.objectContaining({
            status: expect.anything(),
        }),
        );

    })

    test('testing of the call de deleteOneOnlyProduct', async () => {

        const url = await deleteOneOnlyProduct();
        console.warn("hola mundo esto es una prueba para un test");
        console.warn(url.data);

        expect(url).toEqual(expect.objectContaining({
            status: expect.anything(),
            // error: expect.anything()
        }),
        );

    })


})