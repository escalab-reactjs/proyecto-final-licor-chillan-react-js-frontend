import { getOneShoppingCart, getAllShoppingCart, createOneShoppingCart, updateOneShoppingCart, deleteOneShoppingCart } from "../../arrange/conexion/async-await-shoppingCart"


describe('call to data base of PROMOTION', () => {

    test('testing of the call de getOneShoppingCart', async () => {

        const url = await getOneShoppingCart();
        console.warn("hola mundo esto es una prueba para un test");
        console.warn(url.data);

        expect(url).toEqual(expect.objectContaining({
            status: expect.anything(),
        }),
        );

    })

    test('testing of the call de getAllShoppingCart', async () => {

        const url = await getAllShoppingCart();
        console.warn("hola mundo esto es una prueba para un test");
        console.warn(url.data);

        expect(url).toEqual(expect.objectContaining({
            status: expect.anything(),
        }),
        );

    })

    test('testing of the call de createOneShoppingCart', async () => {

        const url = await createOneShoppingCart();
        console.warn("hola mundo esto es una prueba para un test");
        console.warn(url.data);

        expect(url).toEqual(expect.objectContaining({
            status: expect.anything(),
        }),
        );

    })

    test('testing of the call de updateOneShoppingCart', async () => {

        const url = await updateOneShoppingCart();
        console.warn("hola mundo esto es una prueba para un test");
        console.warn(url);

        expect(url).toEqual(expect.objectContaining({
            status: expect.anything(),
        }),
        );

    })

    test('testing of the call de deleteOneShoppingCart', async () => {

        const url = await deleteOneShoppingCart();
        console.warn("hola mundo esto es una prueba para un test");
        console.warn(url.data);

        expect(url).toEqual(expect.objectContaining({
            status: expect.anything(),
        }),
        );

    })


})