const back_url = 'http://localhost:3000/atlas/api/v1';
const promotion_url = '/promotion'

export { back_url };
export const getActivePromotion = () => `${back_url}${promotion_url}/getActivePromotion`;
// export const trackSearch = q_track => `${cors_anywhere}${base_url}${track_search}${q_track}${track_search_params}${api_key}`;