import React, { Fragment, useContext } from "react";
import { HomeContext } from "./../../contexts/HomeContext";
import Promotions from "./Promotions";
import ProgressBar from "./../Common/ProgressBar";
import Message from "./../Common/Message";

const Home = () => {
    const { doneFetch, text, promotions } = useContext(HomeContext);
    return (
        <Fragment>
            {doneFetch ? (
                <Promotions text={text} promotions={promotions} />
            ) : (
                <Message text={text} />
            )}
        </Fragment>
    );
};

export default Home;
