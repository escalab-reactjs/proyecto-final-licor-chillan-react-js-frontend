import React from "react";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import ButtonPrimary from "./../../components/Common/ButtonPrimary";

const Promotion = ({ _id, name, cost_promotion, price_promotion, url_img }) => (
    <Grid item xs={12} sm={6}>
        <Paper className="defaultPaper">
            <h3>{name}</h3>
            <ul>
                <li>
                    <img alt="promotion" src={url_img} />
                    <strong>Promociones:</strong>
                    <span>{name}</span>
                </li>
                {/* <li>
          <ButtonPrimary type="lyrics" to={`/lyrics/track/${commontrack_id}`} />
        </li> */}
            </ul>
        </Paper>
    </Grid>
);

Promotion.displayName = "Promotion";

export default Promotion;
