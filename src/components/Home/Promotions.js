import React, { Fragment } from "react";
import Grid from "@material-ui/core/Grid";
import Promotion from "./Promotion";
import Message from "./../../components/Common/Message";

const Promotions = ({ Promotions, text }) => (
    <Fragment>
        <Message text={text} />
        <div className="root">
            <Grid container spacing={3} justify="center">
                {Promotions.map((Promotion) => {
                    const {
                        _id,
                        name,
                        cost_promotion,
                        price_promotion,
                        url_img,
                    } = Promotion.Promotion;
                    return (
                        <Promotion
                            _id={_id}
                            name={name}
                            cost_promotion={cost_promotion}
                            price_promotion={price_promotion}
                            url_img={url_img}
                        />
                    );
                })}
            </Grid>
        </div>
    </Fragment>
);

export default Promotions;
