import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import HomeContextProvider from "./contexts/HomeContext";
import Header from "./components/Common/Header";
import Home from "./components/Home";
import NotFound from "./components/NotFound";


const App = () => (
  <BrowserRouter>
    <Header />
    <Switch>
      <Route exact path="/">
        <HomeContextProvider>
          <Home />
        </HomeContextProvider>
      </Route>
      <Route component={NotFound} />
    </Switch>
  </BrowserRouter>
);

export default App;
