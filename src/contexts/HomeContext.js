import React, { createContext, useState, useEffect } from 'react';

import { getActivePromotion } from './../constants';

export const HomeContext = createContext();

const HomeContextProvider = ({ children }) => {
    const [doneFetch, setDoneFetch] = useState();
    const [text, setText] = useState('Top Promociones');
    const [promotions, setPromotions] = useState([]);

    useEffect(() => activePromotion(), []);

    const activePromotion = () => {
        fetch(getActivePromotion())
            .then(res => res.json())
            .then(data => {
                setDoneFetch(true);
                setPromotions(data.data);
            })
            .catch(err => console.log(err));
    }

    return (
        < HomeContext.Provider value={{ doneFetch, text, promotions }} >
            { children}
        </HomeContext.Provider>
    );
};
//hola esto es una prueba

export default HomeContextProvider;